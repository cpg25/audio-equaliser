#/*
#    FreeRTOS V7.4.2 - Copyright (C) 2013 Real Time Engineers Ltd.
#	
#
#    ***************************************************************************
#     *                                                                       *
#     *    FreeRTOS tutorial books are available in pdf and paperback.        *
#     *    Complete, revised, and edited pdf reference manuals are also       *
#     *    available.                                                         *
#     *                                                                       *
#     *    Purchasing FreeRTOS documentation will not only help you, by       *
#     *    ensuring you get running as quickly as possible and with an        *
#     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
#     *    the FreeRTOS project to continue with its mission of providing     *
#     *    professional grade, cross platform, de facto standard solutions    *
#     *    for microcontrollers - completely free of charge!                  *
#     *                                                                       *
#     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
#     *                                                                       *
#     *    Thank you for using FreeRTOS, and thank you for your support!      *
#     *                                                                       *
#    ***************************************************************************
#
#
#    This file is part of the FreeRTOS distribution.
#
#    FreeRTOS is free software; you can redistribute it and/or modify it under
#    the terms of the GNU General Public License (version 2) as published by the
#    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
#    >>>NOTE<<< The modification to the GPL is included to allow you to
#    distribute a combined work that includes FreeRTOS without being obliged to
#    provide the source code for proprietary components outside of the FreeRTOS
#    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#    more details. You should have received a copy of the GNU General Public
#    License and the FreeRTOS license exception along with FreeRTOS; if not it
#    can be viewed here: http://www.freertos.org/a00114.html and also obtained
#    by writing to Richard Barry, contact details for whom are available on the
#    FreeRTOS WEB site.
#
#    1 tab == 4 spaces!
#
#    http://www.FreeRTOS.org - Documentation, latest information, license and
#    contact details.
#
#    http://www.SafeRTOS.com - A version that is certified for use in safety
#    critical systems.
#
#    http://www.OpenRTOS.com - Commercial support, development, porting,
#    licensing and training services.
#*/


#/************************************************************************* 
# * Please ensure to read http://www.freertos.org/portLM3Sxxxx_Eclipse.html
# * which provides information on configuring and running this demo for the
# * various Luminary Micro EKs.
# *************************************************************************/

RTOS_SOURCE_DIR=./FreeRTOS
DEMO_COMMON_DIR=./Demo
DEMO_INCLUDE_DIR=./Demo
#UIP_COMMON_DIR=../../Common/ethernet/uIP/uip-1.0/uip
LUMINARY_DRIVER_DIR=./Luminary
ENCE463_SOURCE_DIR=./Source
CMSIS_SOURCE_DIR=./CMSIS
CMSIS_INCLUDE_DIR=./CMSIS/Include

CC=arm-none-eabi-gcc
AS=arm-none-eabi-as
OBJCOPY=arm-none-eabi-objcopy
LDSCRIPT=standalone.ld

# should use --gc-sections but the debugger does not seem to be able to cope with the option.
LINKER_FLAGS=-nostartfiles -Xlinker -oEQUALISER.axf -Xlinker -M -Xlinker -Map=EQUALISER.map -Xlinker --no-gc-sections -lm

DEBUG=-g
OPTIM=-O0

AFLAGS=$(DEBUG) -mthumb -mcpu=cortex-m3


CFLAGS=$(DEBUG) -I . -I $(RTOS_SOURCE_DIR)  \
		-D GCC_ARMCM3_LM3S102 -D inline= -mthumb -mcpu=cortex-m3 $(OPTIM) -T$(LDSCRIPT) \
		-D PACK_STRUCT_END=__attribute\(\(packed\)\) -D ALIGN_STRUCT_END=__attribute\(\(aligned\(4\)\)\) -D sprintf=usprintf -D snprintf=usnprintf -D printf=uipprintf \
		-I $(UIP_COMMON_DIR)  -ffunction-sections -fdata-sections -I $(LUMINARY_DRIVER_DIR) -I $(ENCE463_SOURCE_DIR) \
		-D ARM_MATH_CM3 -I $(CMSIS_INCLUDE_DIR)

SOURCE=	equaliser.c \
		$(LUMINARY_DRIVER_DIR)/rit128x96x4.c \
		$(LUMINARY_DRIVER_DIR)/ustdlib.c \
		$(RTOS_SOURCE_DIR)/list.c \
		$(RTOS_SOURCE_DIR)/queue.c \
		$(RTOS_SOURCE_DIR)/tasks.c \
		$(LUMINARY_DRIVER_DIR)/port.c \
		$(LUMINARY_DRIVER_DIR)/heap_2.c \
		$(LUMINARY_DRIVER_DIR)/class-d-adam.c \
		$(ENCE463_SOURCE_DIR)./audiosampler.c \
		$(ENCE463_SOURCE_DIR)./button.c \
		$(ENCE463_SOURCE_DIR)./logic.c \
		$(ENCE463_SOURCE_DIR)./FFT.c \
		$(ENCE463_SOURCE_DIR)./speaker.c \
		$(ENCE463_SOURCE_DIR)./display.c
		
		
				
ASMSOURCE= 	$(ENCE463_SOURCE_DIR)/Eg_FFT128Real_32b.s \
			$(ENCE463_SOURCE_DIR)/Eg_Magnitude.s \
			$(ENCE463_SOURCE_DIR)/Eg_Window16to32b_real.s 
			
			

LIBS= $(LUMINARY_DRIVER_DIR)/libdriver.a $(LUMINARY_DRIVER_DIR)/libgr.a  $(CMSIS_SOURCE_DIR)/Lib/GCC/libarm_cortexM3l_math.a

OBJS = $(SOURCE:.c=.o)

ASMOBJS = $(ASMSOURCE:.s=.o)

all: EQUALISER.bin
	 
EQUALISER.bin : EQUALISER.axf
	$(OBJCOPY) EQUALISER.axf -O binary EQUALISER.bin

EQUALISER.axf : $(OBJS) $(ASMOBJS) startup.o Makefile
	$(CC) $(CFLAGS) $(OBJS) $(ASMOBJS) startup.o $(LIBS) $(LINKER_FLAGS)

$(OBJS) : %.o : %.c Makefile FreeRTOSConfig.h
	$(CC) -c $(CFLAGS) $< -o $@

$(ASMOBJS) : %.o : %.s Makefile
	$(AS) $(AFLAGS) $< -o $@

startup.o : startup.c Makefile
	$(CC) -c $(CFLAGS) -O1 startup.c -o startup.o
		
clean :
	touch Makefile
	rm $(OBJS)
	rm $(ASMOBJS)
	rm *.axf
	rm *.bin
	rm *.map

################################################################################
# The following modifications by Adam Goodwin.                                 #
################################################################################

# Specify the batch file for running OpenOCD here (extension not needed):
OPENOCD = run_openocd

# Target "openocd". Runs OpenOCD with a specified configuration file. Use the
# command "make openocd". Ensure the board is connected between to the computer.
# Note: OpenOCD is only started if it is not already running.
.PHONY: openocd
openocd:
	@echo Running OpenOCD if it is not already running...
	-@cmd /C $(OPENOCD)
	@echo Done. OpenOCD is running.

# Target "program". Programs the device. Use the command "make program". Note
# that OpenOCD will be started if it is not already running.
.PHONY: program
program: all openocd
	@arm-none-eabi-gdb --batch \
	-ex "target remote localhost:3333" \
	-ex "monitor adapter_khz 500" \
	-ex "monitor reset init" \
	-ex "monitor adapter_khz 500" \
	-ex "load" \
	-ex "monitor adapter_khz 500" \
	-ex "monitor reset init" \
	-ex "monitor resume" \
	EQUALISER.axf


