/**
 * @file 	equaliser.c
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	7 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 *  This program implements an audio equaliser on the Stellaris LM3S1968 Evaluation board. The
 *  program is scheduled using the FREERTOS real time operating system. The program functions within
 *  6 core modules. 
 *
 *	Modules:
 *	audiosampler 	- In charge of sampling and ping-pong buffering
 *	FFT				- In charge of the digital signal processing of the system
 *	button			- In charge of user inputs to the systems
 *	logic			- In charge of interpreting user inputs and send the appropriate messages out
 *	speaker			- In charge of playing audio out after processing
 *	display			- In charge of displaying the current state of the program to the user
 *
 * These modules can be found in the functional block diagram included.
 */

/*************************************************************************

http://www.FreeRTOS.org - Documentation, books, training, latest versions,
license and Real Time Engineers Ltd. contact details.

http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
including FreeRTOS+Trace - an indispensable productivity tool, and our new
fully thread aware and reentrant UDP/IP stack.

*************************************************************************/


/* Standard includes. */
#include <stdio.h>
#include <unistd.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Hardware library includes. */
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_sysctl.h"
#include "sysctl.h"
#include "gpio.h"
#include "grlib.h"
#include "rit128x96x4.h"

/* Module Includes */
#include "mailbox.h"
#include "audiosampler.h"
#include "adc.h"
#include "class-d-adam.h"
#include "button.h"
#include "logic.h"
#include "FFT.h"
#include "speaker.h"
#include "display.h"

/*-----------------------------------------------------------*/

/* The time between cycles of the 'check' functionality (defined within the
tick hook. */
#define mainCHECK_DELAY						( ( portTickType ) 5000 / portTICK_RATE_MS )

/* Size of the stack allocated to the uIP task. */

/* The OLED task uses the sprintf function so requires a little more stack too. */
#define mainOLED_TASK_STACK_SIZE			( configMINIMAL_STACK_SIZE + 50 )

#define mainSPEAKER_TASK_STACK_SIZE			( configMINIMAL_STACK_SIZE )
#define mainBUTTON_TASK_STACK_SIZE			( configMINIMAL_STACK_SIZE )
#define mainLOGIC_TASK_STACK_SIZE			( configMINIMAL_STACK_SIZE )
#define mainFFT_TASK_STACK_SIZE				( configMINIMAL_STACK_SIZE + 5000)

/* Task priorities. */
//TODO Add priorites.
#define mainQUEUE_POLL_PRIORITY				( tskIDLE_PRIORITY + 2 )


/*
 * Configure the hardware for the equaliser
 */
static void prvSetupHardware( void );

int main( void )
{
	prvSetupHardware();
	vAudiosamplerInit();
	vButtonInit();
	vSpeakerInit();
	vDisplayInit();
	vLogicInit();
	vFFTInit();

	/* Start the tasks defined within this file/specific to this demo. */
	xTaskCreate( vDisplayTask, ( signed portCHAR * ) "Display", mainOLED_TASK_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );
	// Button Poll Task
	xTaskCreate( vButtonTask, ( signed portCHAR * ) "Button",  mainBUTTON_TASK_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );
	// Speaker Gateway Task
	xTaskCreate( vSpeakerTask, ( signed portCHAR * ) "Speaker",  mainSPEAKER_TASK_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL );
	// Program State Machine Task
	xTaskCreate( vLogicControlTask, ( signed portCHAR * ) "Logic",  mainLOGIC_TASK_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );
	// FFT Task
	xTaskCreate( vFFTTask, ( signed portCHAR * ) "FFT",  mainFFT_TASK_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL );


	/* Start the scheduler. */
	vTaskStartScheduler();

    /* Will only get here if there was insufficient memory to create the idle
    task. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void prvSetupHardware( void )
{
    /* If running on Rev A2 silicon, turn the LDO voltage up to 2.75V.  This is
    a workaround to allow the PLL to operate reliably. */
    if( DEVICE_IS_REVA2 )
    {
        SysCtlLDOSet( SYSCTL_LDO_2_75V );
    }

	/* Set the clocking to run from the PLL at 50 MHz */
	SysCtlClockSet( SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_8MHZ );
}

/*-----------------------------------------------------------*/


