/**
 * @file 	FFT.c
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	6 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 * This module controls the signal processing of the of the graphical equaliser. It encompasses
 * a Fourier Transform, and Inverse Fourier Transform as well as signal attenuation.
 */

#include <string.h>

#include "FFT.h"

#include "FREERTOS.h"
#include "queue.h"

#include "equaliser.h"
#include "message.h"
#include "speaker.h"
#include "arm_math.h"
#include "mailbox.h"
#include "display.h"
#include "logic.h"

void vMakeSymmetrical( int *cpInputArray, int *cpOutputArray );
void vSplitRealImag ( int *cpInputArray, int* realArray, int* imagArray);
void vFFTSend (xFFTMessage *xMessage, portTickType xDelay);
void magnitude32_32bIn(int   *x,int M);

q15_t xCH1BufferOutA[BUFFER_LEN];
q15_t xCH1BufferOutB[BUFFER_LEN];
q15_t xFFTBuffer[BUFFER_LEN*2];
portLONG plOutputFFTBuffer[BUFFER_LEN*2];
portLONG plAttenuation[NUM_EQ_COLS] = {ATTENUATION_MAX,ATTENUATION_MAX,ATTENUATION_MAX,ATTENUATION_MAX,ATTENUATION_MAX,ATTENUATION_MAX,ATTENUATION_MAX,ATTENUATION_MAX};

q15_t *xpFFT;
q15_t *xpFFTch1;
q15_t *xpFFTch2;

tBoolean isBuffer1 = true;
xQueueHandle xFFTQueue;

const portLONG plNumEqualiserColsFFT = NUM_EQ_COLS;
const portLONG plFrqsPerBinFFT = BUFFER_LEN/NUM_EQ_COLS;

void vFFTInit ( void )
{
	xFFTQueue = xQueueCreate ( mailboxFFT_OUTBOX_LEN, mailboxFFT_OUTBOX_SIZE );
}

void vFFTTask ( void *pvParameters )
{
	/************************************************************************************/
	/*																					*/
	/* Instantiating variables used for the FFT and IFFT. 								*/
	/* See the ARM CMSIS DSP Software Library Documentation for more information.		*/
	/* 																					*/
	/* http://www.keil.com/pack/doc/arm/cmsis/cmsis/documentation/dsp/html/index.html	*/
	/*																					*/
	/************************************************************************************/
	uint32_t xfftSize = BUFFER_LEN;
	uint32_t xfftFlag = 0;
	uint32_t xifftFlag = 1;
	uint32_t xDoBitReverse = 1;
	arm_rfft_instance_q15 xS;
 	arm_cfft_radix4_instance_q15  xS_CFFT;
 	arm_rfft_instance_q15 xSInv;
 	arm_cfft_radix4_instance_q15  xSInv_CFFT;
 	arm_rfft_init_q15 (&xS    , &xS_CFFT    , xfftSize , xfftFlag  , xDoBitReverse);
	arm_rfft_init_q15 (&xSInv , &xSInv_CFFT , xfftSize , xifftFlag , xDoBitReverse);

	xADCMessage xMessageADC;
	xSpeakerMessage xMessageSpeaker;
	xFFTMessage xMessageFFT;
	xLogicMessage xMessageLogic;

	portBASE_TYPE xRetVal = 0;

	int i = 0;
	int j = 0;
	for (;;)
	{
		/* Block until a message arrives from the ADC */
		xADCRecieve(&xMessageADC , portMAX_DELAY);

		/* Grab a message from the logic controller, if empty continue on anyway */
		if (xLogicRecieve(&xMessageLogic, 0))
		{
			for(i = 0; i < NUM_EQ_COLS; i++)
			{
				plAttenuation[i] = xMessageLogic.plAttenuationVals[i];
			}
			xRetVal = 1;
		}

		/* Pointers to channels one and two */
		xpFFTch1 = (q15_t*)xMessageADC.ptrch1;
		xpFFTch2 = (q15_t*)xMessageADC.ptrch2;

		/* Set the FFT pointer to channel whatever channel is currently selected */
		if (xRetVal)
		{
			if (xMessageLogic.plChannel == CHANNEL_ONE)
			{
				xpFFT = xpFFTch1;
			}
			else
			{
				xpFFT = xpFFTch2;
			}
		}
		else
		{
			xpFFT = xpFFTch1;
		}

		/* Shift samples up to 16 bits from 10 bits */
		for (i=0 ; i < BUFFER_LEN ; i++)   xpFFT[i] = xpFFT[i]<<5;

		/* Do an FFT on the incoming buffer and store it in a local buffer */
		arm_rfft_q15(&xS, xpFFT, xFFTBuffer);

		/* Copy contents of half the FFT buffer to an attenuate buffer (FFT's on real signals are symmetric */
		for (i=0 ; i < 2*BUFFER_LEN ; i++)   plOutputFFTBuffer[i] = (portLONG)xFFTBuffer[i];



		/* Convert complex output to magnitude, DC and Fs/2 bins are already real (can be negative!) */
		magnitude32_32bIn((int *)&plOutputFFTBuffer[2],BUFFER_LEN-1);

		/*zero imag part of original complex frequency */
		for (i=0;i< 2*BUFFER_LEN;i+=2) plOutputFFTBuffer[i+1]=0;

		/* 2Q30 converted to 17Q15 and rounded */
		//for (i=0;i< BUFFER_LEN;i+=2) plOutputFFTBuffer[i]=((plOutputFFTBuffer[i]+16384)>>15);

		/* Send away the plAtttenauteBuffer to the screen */
		xMessageFFT.plFreqVals = plOutputFFTBuffer;
		xFFTSend(&xMessageFFT, 0);

		/* Attenuate frequency bands of the incoming signal by multiplying the real and imaginary */
		/* components by a user provided constant level. 										  */
		/* Skip the DC Component of the FFT during attenuation. 								  */
		for (i= 2; i < BUFFER_LEN; i++)
		{
			xFFTBuffer[i] = (xFFTBuffer[i]*plAttenuation[i/plFrqsPerBinFFT])/100;
			xFFTBuffer[i+1] = (xFFTBuffer[(i+1)]*plAttenuation[(i+1)/plFrqsPerBinFFT])/100;
		}



		//for (i= 2; i <BUFFER_LEN/2; i++) xFFTBuffer[i] = 0;

		/* Check which buffer is currently incoming and fill the corresponding output buffer */
		if (isBuffer1)
		{
			/* Do an IFFT on the local buffer and store it in a separate output buffer */
			arm_rfft_q15(&xSInv, xFFTBuffer , xCH1BufferOutA);

			/* Shift the samples up by as the FFT->IFFT reduces the output by a factor of 6 	 */
			/* turn these signals back to 10 bit samples. Hence left shift each sample by 6-5=1. */
			for (i=0;i<BUFFER_LEN;i++)   xCH1BufferOutA[i]=xCH1BufferOutA[i]<<1;

			/* Set the outgoing pointer to the new buffer */
			xMessageSpeaker.ptraudio = (void*)xCH1BufferOutA;
		}
		else
		{
			/* Do an IFFT on the local buffer and store it in a separate output buffer */
			arm_rfft_q15(&xSInv, xFFTBuffer , xCH1BufferOutB);

			/* Shift the samples up by as the FFT->IFFT reduces the output by a factor of 6 	 */
			/* turn these signals back to 10 bit samples. Hence left shift each sample by 6-5=1. */
			for (i=0;i<BUFFER_LEN;i++)   xCH1BufferOutB[i]=xCH1BufferOutB[i]<<1;

			/* Set the outgoing pointer to the new buffer */
			xMessageSpeaker.ptraudio = (void*)xCH1BufferOutB;

		}

		/* Finally tell the Speaker about the new buffer */
		vSpeakerSend (&xMessageSpeaker, 0);
		isBuffer1 = !isBuffer1;
	}
}

portBASE_TYPE xFFTSend (xFFTMessage *xMessage, portTickType xDelay)
{
	return xQueueSend(xFFTQueue, xMessage, xDelay);
}

portBASE_TYPE xFFTRecieve (xFFTMessage *xMessage, portTickType xDelay)
{
	return xQueueReceive(xFFTQueue, xMessage, xDelay);
}

/***********************************************************************************************************/
/* THESE FUNCTIONS SHOULD NOT BE USED */

void vFFT( int *cpInputArray, int *cpOutputArray)
{
	int i = 0;
	unsigned short w[BUFFER_LEN/2]; //first half of symetrical window 0Q16 unsigned

	//create windowing function 0Q16  (0.5 =32768   0.99998 =65535)
	for (i=0;i<BUFFER_LEN/2;i++)   w[i]=0xffff;   //0.99998

	//clear output array
	for (i=0;i<BUFFER_LEN;i++)   cpOutputArray[i]=0;

	//Window16to32b_real( cpInputArray, Hamming128_16b, NN); // apply Hamming window to input signal
	Window16to32b_real( cpInputArray, w, BUFFER_LEN); //or just expand to 32 bits with 1/2 scale

	//call FFT routine;
	FFT128Real_32b(cpOutputArray,cpInputArray);
}

//TODO Input size variable NN
void vInvFFT( int *cpInputArray, int *cpOutputArray )
{
	int cpInputArraySym[BUFFER_LEN*2] = { 0 };

	int real[BUFFER_LEN] = { 0 };
	int imag[BUFFER_LEN] = { 0 };

	int real2[BUFFER_LEN] = { 0 };
	int imag2[BUFFER_LEN] = { 0 };

	int realfft[BUFFER_LEN] = { 0 };
	int imagfft[BUFFER_LEN] = { 0 };

	int cpInputArraySwap[BUFFER_LEN*2] = { 0 };

	int realfftsym[BUFFER_LEN*2] = { 0 };
	int imagfftsym[BUFFER_LEN*2] = { 0 };

	vMakeSymmetrical(cpInputArray,cpInputArraySym);
	vSplitRealImag(cpInputArraySym, real, imag);

	FFT128Real_32b(realfft,real);
	vMakeSymmetrical(realfft,realfftsym);

	FFT128Real_32b(imagfft,imag);
	vMakeSymmetrical(imagfft,imagfftsym);

	int i = 0;
	for (i = 0; i < BUFFER_LEN; i++)
	{
		cpInputArraySwap[2*i] = imagfftsym[2*i] - realfftsym[2*i+1];
		cpInputArraySwap[2*i+1] = realfftsym[2*i] + imagfftsym[2*i+1] ;
	}

	vSplitRealImag(cpInputArraySwap, real2, imag2);

	for (i = 0; i < BUFFER_LEN; i++)
	{
		cpOutputArray[2*i] = imag2[i];
		cpOutputArray[2*i+1] = real2[i];
	}

	for (i = 0; i < 2*BUFFER_LEN; i++) cpOutputArray[i] = cpOutputArray[i]/BUFFER_LEN;

}

void vMakeSymmetrical( int *cpInputArray, int *cpOutputArray )
{
	int i = 0;
	//Clearing Output Array
	for (i=0;i<2*BUFFER_LEN;i++)   cpOutputArray[i]=0;

	//filling first half of the array with the FFT values
	for (i = 0; i < BUFFER_LEN+2; i++)
	{
		cpOutputArray[i] = cpInputArray[i];
	}

	//Flipping the input array into the second half of symetrical array.
	for (i = 0; i < BUFFER_LEN/2-1; i++)
	{
		cpOutputArray[BUFFER_LEN+2+2*i] = cpInputArray[BUFFER_LEN-2*i-2];
		cpOutputArray[BUFFER_LEN+3+2*i] = -cpInputArray[BUFFER_LEN-2*i-1];
	}


}

void vSplitRealImag ( int *cpInputArray, int* realArray, int* imagArray)
{
	int i = 0;
	for (i = 0; i < BUFFER_LEN; i++)
	{
		realArray[i] = cpInputArray[i*2];
		imagArray[i] = cpInputArray[i*2+1];
	}
}

