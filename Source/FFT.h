/**
 * @file 	FFT.h
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	6 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 * This module controls the signal processing of the of the graphical equaliser. It encompasses
 * a Fourier Transform, and Inverse Fourier Transform as well as signal attenuation.
 */

#ifndef FFT_H_
#define FFT_H_

#include "FREERTOS.h"
#include "message.h"

/**
 * Initialises the FFT Task.
 */
void vFFTInit ( void );

/**
 * This Task computes the Fast fourier transform of length defined in equaliser.h
 * It sends away the FFT values to the sceen for printing. The task then atennuates 
 * frequencies that the user selects in the user interface. Finally the task computes
 * the inverse fourier transform of the attenuated signal and sends it to the speaker.
 * 
 * @param pvParameters Parameters can be passed into the task when the task is created in FREERTOS. Currently
 * 			               this task will ignore any parameters
 */
void vFFTTask ( void *pvParameters );

/**
 * Computes the Fast fourier Transform of a real time domain signal and puts the
 * resultant FFt in the output array. only half the FFT is put into the output array
 * as a real FFT is symmetrical.
 * This function should not be used as it is too slow.
 *  
 * @param cpInputArray  Slice of a real time domain signal 
 * 
 * @param cpOutputArray Destination Array which contains complex data of the FFT.
 */
void vFFT( int *cpInputArray, int *cpOutputArray);

/**
 * Computes the Inverse Fast fourier Transform of a real time domain signal and puts the
 * resultant FFT in the output array.
 * This function should not be used as it is too slow.
 *  
 * @param cpInputArray  Complex FFT data
 * 
 * @param cpOutputArray Destination Array which contains real data from the IFFT.
 */

void vInvFFT( int *cpInputArray, int *cpOutputArray );

/**
 * Sends a message from the FFT module to tell another module 
 * 
 * @param xMessage		A message struct containing the FFT values.
 * 						The format is specified in the message.h header file.
 * 						
 * @param xDelay		A value for how long the the function is to wait for a space in the FFT
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a space
 * 						is free. A value of 0 will return immediately regardless of whether or not the
 * 						mailbox is free.
 *
 * @return 				Returns an error type errQUEUE_FULL if the message could not be added to the
 * 						mailbox or a pdPASS if the receive was successful.
 */
portBASE_TYPE xFFTSend (xFFTMessage *xMessage, portTickType xDelay);

/**
 * Recieves a message from the FFT module indicating that a buffer is ready for processing.
 * 
 * @param xMessage		A message struct containing the FFT values.
 * 						The format is specified in the message.h header file.
 * 	
 * @param xDelay		A value for how long the the function is to wait for a message to arrive in the FFT
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a message is
 * 						received. A value of 0 will return immediately regardless of whether or not the
 * 						message was received.
 * 						
 * @return 				Returns an error type errQUEUE_EMPTY if a message was not received from the
 * 						mailbox or a pdPASS if the receive was successful.
 */
portBASE_TYPE xFFTRecieve (xFFTMessage *xMessage, portTickType xDelay);

#endif /* FFT_H_ */
