/**
 * @file 	audiosampler.h
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	5 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 * This module controls the sampling of an audio input. It implements a ping-pong buffer structure
 * for each audio channel. The module is configured for use on the Stellaris LM3S1968 Evaluation Kit.
 */

#ifndef SAMPLER_H
#define SAMPLER_H

#include "message.h"
#include "FREERTOS.h"

/**
 * Initialises the Analog to Digital converter peripheral for the Stellaris LM3S1968 
 * Evaluation board. The ADC is configured to sequence sample two channel audio on 
 * the ADC0 and ADC7 breakout pins. The ADC samples on a timer interrupt at a rate specified 
 * by the user in the equaliser.h header file.
 */
void vAudiosamplerInit( void );

/**
 * The interrupt handler for the ADC. A ping-pong buffering approach is used. A buffer with a
 * buffer length defined in the equaliser.h is filled followed by a second buffer of the same
 * buffer length. When a buffer is filled, a message is added to the ADC mailbox which can be
 * received by a module using the xADCRecieive function.
 */
void vAudiosamplerIntHandler( void );


/**
 * Sends a message from the ADC to tell another module that a buffer is ready for processing.
 * 
 * @param xMessage		A message struct containing pointers to the current buffers for channel
 * 						one and channel two. The format is specified in the message.h header file.
 * 						
 * @param xDelay		A value for how long the the function is to wait for a space in the ADC
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a space
 * 						is free. A value of 0 will return immediately regardless of whether or not the
 * 						mailbox is free.
 *
 * @return 				Returns an error type errQUEUE_FULL if the message could not be added to the
 * 						mailbox or a pdPASS if the receive was successful.
 */
portBASE_TYPE xADCSend (xADCMessage *xMessage, portTickType xDelay);

/**
 * Recieves a message from the ADC indicating that a buffer is ready for processing.
 * 
 * @param xMessage		A message struct containing pointers to the current buffers for channel
 * 						one and channel two. The format is specified in the message.h header file.
 * 	
 * @param xDelay		A value for how long the the function is to wait for a message to arrive in the ADC
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a message is
 * 						received. A value of 0 will return immediately regardless of whether or not the
 * 						message was received.
 * 						
 * @return 				Returns an error type errQUEUE_EMPTY if a message was not received from the
 * 						mailbox or a pdPASS if the receive was successful.
 */
portBASE_TYPE xADCRecieve (xADCMessage *xMessage, portTickType xDelay);

#endif
