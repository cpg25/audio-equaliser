/**
 * @file 	mailbox.h
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	5 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 *  The sizes of all the different mailboxes.
 */

#ifndef MAILBOX_H_
#define MAILBOX_H_

#include "message.h"

/**
 * @def mailboxBUTTON_OUTBOX_LEN
 * @brief The length of the Button Module Outbox
 */
#define mailboxBUTTON_OUTBOX_LEN 3
#define mailboxBUTTON_OUTBOX_SIZE sizeof( xButtonMessage )
/**
 * @def mailboxADC_OUTBOX_LEN
 * @brief The length of the ADC Module Outbox
 */
#define mailboxADC_OUTBOX_LEN 3
#define mailboxADC_OUTBOX_SIZE sizeof( xADCMessage )

/**
 * @def mailboxSPEAKER_INBOX_LEN
 * @brief The length of the Speaker Module Inbox
 */
#define mailboxSPEAKER_INBOX_LEN 3
#define mailboxSPEAKER_INBOX_SIZE sizeof( xSpeakerMessage )

/**
 * @def mailboxDISPLAY_INBOX_LEN
 * @brief The length of the Display Module Inbox
 */
#define mailboxDISPLAY_INBOX_LEN 3
#define mailboxDISPLAY_INBOX_SIZE sizeof( xDisplayMessage )
/**
 * @def mailboxFFT_OUTBOX_LEN
 * @brief The length of the FFT Module Outbox
 */
#define mailboxFFT_OUTBOX_LEN 3
#define mailboxFFT_OUTBOX_SIZE sizeof( xFFTMessage )
/**
 * @def mailboxLOGIC_OUTBOX_LEN
 * @brief The length of the LOGIC Module Outbox
 */
#define mailboxLOGIC_OUTBOX_LEN 3
#define mailboxLOGIC_OUTBOX_SIZE sizeof( xLogicMessage )

#endif /* MAILBOX_H_ */
