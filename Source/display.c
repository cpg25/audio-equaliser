/**
 * @file 	display.h
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	9 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 * This module controls the display on the LM3S1968. It recieves messages from the FFT module
 * and the logic controller and displays the appropriate equalisation output.
 */

#include "display.h"
#include "equaliser.h"

#include "FREERTOS.h"
#include "queue.h"

#include "mybitmap.h"
#include "mailbox.h"
#include "message.h"

/* Constants used when writing strings to the display. */
#define displayCHARACTER_HEIGHT					( 9 )
#define displayMAX_ROWS_128						( displayCHARACTER_HEIGHT * 14 )
#define displayMAX_ROWS_96						( displayCHARACTER_HEIGHT * 10 )
#define displayMAX_ROWS_64						( displayCHARACTER_HEIGHT * 7 )
#define displayFULL_SCALE						( 15 )
#define displayEQBAR_YSPACING					( 1 )
#define displayEQBAR_XSPACING					( 2 )
#define displayEQBAR_YPOS						( 12 )
#define displayEQBAR_XPOS						( 0 )
#define ulSSI_FREQUENCY							( 3500000UL )

/* Number of Equaliser Bars */
const portLONG plNumEqualiserRows = NUM_EQ_ROWS;
const portLONG plNumEqualiserCols = NUM_EQ_COLS;
const portLONG plFrqsPerBin = BUFFER_LEN/NUM_EQ_COLS;

/* Array of previous Values of an FFT */
portLONG xEQAverage[NUM_EQ_COLS];

/* The screen title. */
const portCHAR const *pcTitle = "  ENCE463 EQUALISER  ";

/* Display Queue */
xQueueHandle xDisplayQueue;

void EQDraw (int x, int y, int width, int height, const unsigned char colour);

void vDisplayInit (void)
{
	RIT128x96x4Init( ulSSI_FREQUENCY );
	xDisplayQueue = xQueueCreate ( mailboxDISPLAY_INBOX_LEN, mailboxDISPLAY_INBOX_SIZE );
}

void vDisplayTask( void *pvParameters )
{
	xDisplayMessage xMessageDisplay;
	xFFTMessage xMessageFFT;

	portTickType xLastWakeTime;
	const portTickType xPeriod = (30/ portTICK_RATE_MS );
	portLONG i = 0;
	portLONG j = 0;
	portLONG plTotal = 0;
	portLONG plEQRows = 0;
	portLONG plEQCols = 0;
	portLONG plEQX = 0;
	portLONG plEQY = 0;
	portLONG plPreviousCol = 0;

	portCHAR pcTempMessage[25];

	portLONG plEQBAR_WIDTH	= (displayMAX_ROWS_128-2*displayEQBAR_XPOS			/* Potential width 				*/
								-(plNumEqualiserCols-1)*displayEQBAR_XSPACING) 	/* subtract spacing between bars*/
								/ (plNumEqualiserCols);							/* divide by number of X bars	*/

	portLONG plEQBAR_HEIGHT = (displayMAX_ROWS_96-2*displayEQBAR_YPOS				/* Potential Height				*/
								-(plNumEqualiserRows-1)*displayEQBAR_YSPACING) 	/* subtract spacing between bars*/
								/ (plNumEqualiserRows);							/* divide by number of Y bars	*/

	/* Colour Values for display */
	const unsigned portCHAR pcWhite = 0xff;
	const unsigned portCHAR pcGrey = 0x77;
	const unsigned portCHAR pcBlack = 0x00;

	/* Display a starting screen */
	RIT128x96x4Clear();
	RIT128x96x4ImageDraw( myBitmap, 0, displayCHARACTER_HEIGHT + 1, mybmpBITMAP_WIDTH, mybmpBITMAP_HEIGHT );
	RIT128x96x4StringDraw( " POWERED BY FreeRTOS ", 0, 2*displayCHARACTER_HEIGHT + mybmpBITMAP_HEIGHT, displayFULL_SCALE );

	RIT128x96x4StringDraw( "SELECT = SWAP CHANNEL", 0, 3*displayCHARACTER_HEIGHT + mybmpBITMAP_HEIGHT, displayFULL_SCALE );


	/* Wait one second before loading the equaliser */
	vTaskDelay	(5000/portTICK_RATE_MS);
	RIT128x96x4Clear();

	xLastWakeTime = xTaskGetTickCount();
	for ( ;; )
	{
		/* Update every xPeriod amount of time */
		vTaskDelayUntil( &xLastWakeTime, xPeriod);

		portBASE_TYPE pbRetval = pdPASS;
		/* Clear buffer so the most recent FFT Values are received */
		while (pbRetval)
			pbRetval = xFFTRecieve(&xMessageFFT , 0);

		/* Average the FFT Values and put these averages into an averages buffer */
		for (i=0; i < plNumEqualiserCols; i++)
		{
			plTotal = 0;
			for (j= 0; j < plFrqsPerBin; j++)
			{
				/* Average the frequencies skipping the first two samples */
				plTotal += xMessageFFT.plFreqVals[plFrqsPerBin*i+j+2];
			}
			//When a square wave is input at max amplitude values of ~12k were 		*/
			/* observed, Hence there is a magic number  here to scale the input	*/
			/* to between 100 and 0													*/
			xEQAverage[i] = (plTotal/plFrqsPerBin)>>2;

		}

		RIT128x96x4StringDraw( pcTitle, 0, 0, displayFULL_SCALE );
		for (plEQCols = 0; plEQCols < plNumEqualiserCols; plEQCols++)
		{
			for (plEQRows = 0 ; plEQRows < plNumEqualiserRows ; plEQRows++)
			{
				plEQX = (plEQBAR_WIDTH+displayEQBAR_XSPACING)*plEQCols+displayEQBAR_XPOS;
				plEQY = (plEQBAR_HEIGHT+displayEQBAR_YSPACING)*plEQRows+displayEQBAR_YPOS;

				/* Check if the value for a given fft bin is higher than the weighting 	*/
				/* for the current equaliser bar. If it is greater, show that bar in	*/
				/* white, otherwise show that bar in grey								*/
				if (xEQAverage[plEQCols] > (((plNumEqualiserRows-plEQRows)*100)/plNumEqualiserRows))
				{
					prvEQDraw( plEQX, plEQY, plEQBAR_WIDTH, plEQBAR_HEIGHT, pcWhite );
				}
				else
				{
					prvEQDraw( plEQX, plEQY, plEQBAR_WIDTH, plEQBAR_HEIGHT, pcGrey );
				}
			}
		}
		/* Checking if button input has been pressed */
		portBASE_TYPE pbRetVal = xQueueReceive(xDisplayQueue, &xMessageDisplay, 0);
		if (pbRetVal == pdPASS)
		{
			/* Clear previous selected column */
			plEQX = (plEQBAR_WIDTH+displayEQBAR_XSPACING)*plPreviousCol+displayEQBAR_XPOS;
			plEQY = (plEQBAR_HEIGHT+displayEQBAR_YSPACING)*plEQRows+1+displayEQBAR_YPOS;
			prvEQDraw( plEQX, plEQY, plEQBAR_WIDTH, plEQBAR_HEIGHT/2, pcBlack );

			/*	Draw new selected column */
			plEQX = (plEQBAR_WIDTH+displayEQBAR_XSPACING)*xMessageDisplay.plSelectedCol+displayEQBAR_XPOS;
			plEQY = (plEQBAR_HEIGHT+displayEQBAR_YSPACING)*plEQRows+1+displayEQBAR_YPOS;
			prvEQDraw( plEQX, plEQY, plEQBAR_WIDTH, plEQBAR_HEIGHT/2, pcGrey );

			plPreviousCol = xMessageDisplay.plSelectedCol;

			/* Draw below Equaliser Bars */
			for (plEQCols = 0; plEQCols < plNumEqualiserCols; plEQCols++)
			{
				plEQX = (plEQBAR_WIDTH+displayEQBAR_XSPACING)*plEQCols+displayEQBAR_XPOS;

				sprintf(pcTempMessage,"%2d",(xMessageDisplay.plAttenuationVals[plEQCols])/10);
				/* Check if the current value being printed is for the column, currently selected.	*/
				/* If so, make this column value highlighted.										*/
				if (plEQCols == xMessageDisplay.plSelectedCol)
				{
					RIT128x96x4StringDraw( pcTempMessage, plEQX, displayMAX_ROWS_96 - displayEQBAR_YPOS , displayFULL_SCALE );
				}
				else
				{
					RIT128x96x4StringDraw( pcTempMessage, plEQX, displayMAX_ROWS_96 - displayEQBAR_YPOS , 4 );
				}
			}
			if (xMessageDisplay.plChannel == CHANNEL_ONE )
			{
				sprintf(pcTempMessage,"0Hz CHANNEL ONE %2dkHz", SAMPLE_RATE/2000);
				RIT128x96x4StringDraw( pcTempMessage, 0 , displayMAX_ROWS_96-2 , displayFULL_SCALE);
			}
			else
			{
				sprintf(pcTempMessage,"0Hz CHANNEL TWO %2dkHz", SAMPLE_RATE/2000);
				RIT128x96x4StringDraw( pcTempMessage, 0 , displayMAX_ROWS_96-2 , displayFULL_SCALE);
			}
		}



	}
}

portBASE_TYPE xDisplaySend (xDisplayMessage *xMessage, portTickType xDelay)
{
	return xQueueSend(xDisplayQueue, xMessage, xDelay);
}

portBASE_TYPE xDisplayRecieve (xDisplayMessage *xMessage, portTickType xDelay)
{
	return xQueueReceive(xDisplayQueue, xMessage, xDelay);
}


void prvEQDraw (int x, int y, int width, int height, const unsigned char colour)
{
	int i;
	char EQBar[(width*height)/2];
	for (i = 0; i<((width*height)/2); i++)
	{
		EQBar[i] = colour;
	}
	RIT128x96x4ImageDraw(EQBar,x,y,width,height);
}

