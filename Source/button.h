/**
 * @file 	button.h
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>, Adam Goodwin <adg59@uclive.ac.nz>
 * @date 	5 / 8 / 2013
 *
 * @copyright	Craig Gray, Adam Goodwin, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 * This button module contains functions for recognising push button presses on the Stellaris
 * LM3S1968 Evaluation Kit. It is implemented using debounced polling.
 */

#ifndef BUTTON_H
#define BUTTON_H

#include "hw_types.h"
#include "gpio.h"
#include "message.h"

 /**
  * @def    BUTTON_PINS
  * @breif 	For referring to the up, down, left, right and select buttons simultaneously.
  * 		(PG3, PG4, PG5, PG6 and PG7 respectively.)
  */
#define BUTTON_PINS (GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7)

/**
 * Some more user friendly names for each button.
 */
typedef enum {
	BUTTON_UP = GPIO_PIN_3,
	BUTTON_DOWN = GPIO_PIN_4,
	BUTTON_LEFT = GPIO_PIN_5,
	BUTTON_RIGHT = GPIO_PIN_6,
	BUTTON_SELECT = GPIO_PIN_7
} button_t;

/**
 * Initialises the buttons on the board. Up, Down, Left, Right and Select.
 */
void vButtonInit(void);


/**
 * Updates the current and previous button states. This should be called before trying to check for button presses.
 */
void vButtonUpdate(void);


/**
 * Used for a FreeRTOS implementation. Publishes messages whenever a button is pressed. This function
 * debounces inputs using a software filter. 
 * 
 * @param pvParameters Parameters can be passed into the task when the task is created in FREERTOS. Currently
 * 			                 this task will ignore any parameters
 */
void vButtonTask ( void *pvParameters );

/**
 * Sends a message from the Button module to tell another module that a button has been pressed.
 * 
 * @param xMessage		A message struct containing a button and the current state of the button. 
 * 						The format is specified in the message.h header file.
 * 						
 * @param xDelay		A value for how long the the function is to wait for a space in the Button
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a space
 * 						is free. A value of 0 will return immediately regardless of whether or not the
 * 						mailbox is free.
 *
 * @return 				Returns an error type errQUEUE_FULL if the message could not be added to the
 * 						mailbox or a pdPASS if the receive was successful.
 */
portBASE_TYPE xButtonSend (xButtonMessage *xMessage, portTickType xDelay);

/**
 * Recieves a message from the Button module indicating that a button has been pressed.
 * 
 * @param xMessage		A message struct containing a button and the current state of thr button.
 * 						The format is specified in the message.h header file.
 * 	
 * @param xDelay		A value for how long the the function is to wait for a message to arrive in the Button
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a message is
 * 						received. A value of 0 will return immediately regardless of whether or not the
 * 						message was received.
 * 						
 * @return 				Returns an error type errQUEUE_EMPTY if a message was not received from the
 * 						mailbox or a pdPASS if the receive was successful.
 */
portBASE_TYPE xButtonRecieve (xButtonMessage *xMessage, portTickType xDelay);

/**
 * A non FREERTOS implementation of button checking. It does not contain debouncing and is depricated.
 * 
 * @param button 	A button defined in the button_t enumerator.
 * @return 			This function returns true if the specified button is pressed down, otherwise false.
 */
tBoolean xButtonDown(button_t button);


/**
 * A non FREERTOS implementation of button checking. It does not contain debouncing and is depricated.
 * 
 * @param  button 	A button defined in the button_t enumerator.
 * @return 			This function returns true if the specified button has just been pressed, 
 * 					but wasn't pressed previously. Otherwise false.
 */
tBoolean xButtonPressed(button_t button);

#endif // BUTTON_H
