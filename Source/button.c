/**
 * @file 	button.c
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>, Adam Goodwin <adg59@uclive.ac.nz>
 * @date 	5 / 8 / 2013
 *
 * @copyright	Craig Gray, Adam Goodwin, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 * This button module contains functions for recognising push button presses on the Stellaris
 * LM3S1968 Evaluation Kit. It is implemented using debounced polling.
 */
#include "FreeRTOS.h"
#include "queue.h"
#include "audiosampler.h"

#include "button.h"
#include "hw_memmap.h"
#include "sysctl.h"
#include "mailbox.h"

#define BUTTON_PRESS_THRESHOLD 20
#define BUTTON_RELEASE_THRESHOLD 0

//Used for non de-bouncing implementation
portCHAR buttonState;
portCHAR previousButtonState;

//Used for de-bouncing implementation
portCHAR pcButtonsCurrentStates[8] = { 0 };
portCHAR pcButtonsPrevStates[8] = { 0 };

//Button Messenger
xQueueHandle xButtonQueue;

void vButtonInit(void)
{
	// Enable the GPIOG peripheral so that buttons can be used.
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);

	// Set up, down, left, right and select buttons' pins as inputs.
	GPIOPinTypeGPIOInput(GPIO_PORTG_BASE, BUTTON_PINS);

	// Set the pad configurations for the buttons' pins.
	GPIOPadConfigSet(GPIO_PORTG_BASE, BUTTON_PINS, GPIO_STRENGTH_2MA,
					 GPIO_PIN_TYPE_STD_WPU);

	// Create the Button Messenger
	xButtonQueue = xQueueCreate ( mailboxBUTTON_OUTBOX_LEN, mailboxBUTTON_OUTBOX_SIZE );
}


void vButtonUpdate(void)
{
	previousButtonState = buttonState;
	buttonState = ~GPIOPinRead(GPIO_PORTG_BASE, BUTTON_PINS);
}


tBoolean xButtonDown(button_t button)
{
	if (buttonState & button)
	{
		return true;
	}
	else
	{
		return false;
	}
}


tBoolean xButtonPressed(button_t button)
{
	if ((buttonState & button) && !(previousButtonState & button))
	{
		return true;
	}
	else
	{
		return false;
	}
}


void vButtonTask ( void *pvParameters )
{
	int i = 0;
	xButtonMessage xMessage;
	portTickType xLastWakeTime;
	const portTickType xPeriod = (3/ portTICK_RATE_MS );
	xLastWakeTime = xTaskGetTickCount();

	for ( ;; )
	{
		//Wait for xPeriod amount of time before polling.
		vTaskDelayUntil( &xLastWakeTime, xPeriod);
		// Check the current state of the buttons.
		vButtonUpdate();
		for (i = 0; i < 7; i++)
		{
			//Picking out Each element of the current button state bitmask.
			//AKA each button.
			portCHAR pcCurrentButton = (buttonState & BUTTON_PINS & (1 << i));
			
			//Cast the current button to a boolean.
			tBoolean xIsPressed = pcCurrentButton != 0;

			//0's are mapped to -1's and 1's are mapped to ones
			//	0 -> -1 				1 -> 1
			// This is used to create a linear filter for the button states.
			pcButtonsCurrentStates[i] += (xIsPressed*2)-1;

			//Check to see if button has exceeded filter bounds
			if (pcButtonsCurrentStates[i] > BUTTON_PRESS_THRESHOLD)
			{
				pcButtonsCurrentStates[i] = BUTTON_PRESS_THRESHOLD;
			}
			else if (pcButtonsCurrentStates[i] < BUTTON_RELEASE_THRESHOLD)
			{
				pcButtonsCurrentStates[i] = BUTTON_RELEASE_THRESHOLD;
			}

			// Check to see if button is pressed and if it wasn't previously pressed.
			// Publish a message if a change has occurred.
			if ((pcButtonsCurrentStates[i] == BUTTON_PRESS_THRESHOLD)
											&& (pcButtonsPrevStates[i] == BUTTON_RELEASE_THRESHOLD))
			{
				pcButtonsPrevStates[i] = BUTTON_PRESS_THRESHOLD;
				xMessage.button = pcCurrentButton;
				xMessage.state = true;
				xButtonSend(&xMessage,portMAX_DELAY);
			}
			// Do the opposite check
			else if ((pcButtonsCurrentStates[i] == BUTTON_RELEASE_THRESHOLD)
												&& (pcButtonsPrevStates[i] == BUTTON_PRESS_THRESHOLD))
			{
				pcButtonsPrevStates[i] = BUTTON_RELEASE_THRESHOLD;
				xMessage.button = pcCurrentButton;
				xMessage.state = false;
				xButtonSend(&xMessage,portMAX_DELAY);
			}
		}
	}
}

//Send a message from the Button queue.
portBASE_TYPE xButtonSend (xButtonMessage *xMessage, portTickType xDelay)
{
	return xQueueSend(xButtonQueue, xMessage, xDelay);
}

//Recieve a message from the Button queue.
portBASE_TYPE xButtonRecieve (xButtonMessage *xMessage, portTickType xDelay)
{
	return xQueueReceive(xButtonQueue, xMessage, xDelay);
}
