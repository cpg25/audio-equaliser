/**
 * @file 	speaker.h
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	7 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 *  This module is in charge of playing the speaker.
 */

#ifndef SPEAKER_H_
#define SPEAKER_H_

#include "FREERTOS.h"
#include "message.h"

/**
 * Intitalises the Speaker. The speaker uses PWM2 as its output. This is configure in class-d-adam.c which
 * is a Stellaris peripheral driver that has been modified by Adam Goodwin <adg59@uclive.ac.nz> and then
 * further modified by Craig Gray <cpg25@uclive.ac.nz>
 */
void vSpeakerInit ( void );

/**
 * The speaker task recieves a buffer and plays it through the class-D amplifer using the Class-D driver.
 * 
 * @param pvParameters Parameters can be passed into the task when the task is created in FREERTOS. Currently
 * 			                 this task will ignore any parameters
 */
void vSpeakerTask (void *pvParameters );

/**
 * Sends a message to the Speaker Module telling it to play a buffer.
 * 
 * @param xMessage		A message struct containing a pointer to a buffer and the length of the buffer.
 * 						The format is specified in the message.h header file.
 * 						
 * @param xDelay		A value for how long the the function is to wait for a space in the Speaker
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a space
 * 						is free. A value of 0 will return immediately regardless of whether or not the
 * 						mailbox is free.
 */

void vSpeakerSend (xSpeakerMessage *xMessage, portTickType xDelay);

/**
 * Recieves a message to the Speaker Module telling it to play a buffer.
 * 
 * @param xMessage		A message struct containing a pointer to a buffer and the length of the buffer.
 * 						The format is specified in the message.h header file.
 * 						
 * @param xDelay		A value for how long the the function is to wait for a space in the Speaker
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a space
 * 						is free. A value of 0 will return immediately regardless of whether or not the
 * 						mailbox is free.
 */
void vSpeakerRecieve (xSpeakerMessage *xMessage, portTickType xDelay);

#endif /* SPEAKER_H_ */
