/**
 * @file 	logic.h
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	6 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 * This module controls all the logic controls of the program. It can be considred the gateway
 * task, taking inputs from modules and passing the the resultant output onto other tasks.
 */

#ifndef LOGIC_H_
#define LOGIC_H_

#include "message.h"

/**
 * @def ATTENUATION_MAX
 * @brief This is the highest values of attenuation that the user can input. It
 *        is a percentage measure.
 */
#define ATTENUATION_MAX 	100

 /**
 * @def ATTENUATION_MIN
 * @brief This is the highest values of attenuation that the user can input. It
 *        is a percentage measure.
 */
#define ATTENUATION_MIN 	0

  /**
 * @def ATTENUATION_STEP
 * @brief This is the the attenuation step that a button press will result in. It
 *        is currently set to 10%
 */
#define ATTENUATION_STEP 	10


void vLogicControlTask ( void *pvParameters );

/**
 * Sends a message from the logic module incating the current state of the program.
 * 
 * @param xMessage		A message struct containing the state of the program.
 * 						The format is specified in the message.h header file.
 * 	
 * @param xDelay		A value for how long the the function is to wait for a message to arrive in the Logic
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a message is
 * 						received. A value of 0 will return immediately regardless of whether or not the
 * 						message was received.
 * 						
 * @return 				Returns an error type errQUEUE_FULL if a message was not received from the
 * 						mailbox or a pdPASS if the receive was successful.
 */
portBASE_TYPE xLogicSend (xLogicMessage *xMessage, portTickType xDelay);

/**
 * Recieves  a message from the logic module incating the current state of the program.
 * 
 * @param xMessage		A message struct containing the state of the program.
 * 						The format is specified in the message.h header file.
 * 	
 * @param xDelay		A value for how long the the function is to wait for a message to arrive in the Logic
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a message is
 * 						received. A value of 0 will return immediately regardless of whether or not the
 * 						message was received.
 * 						
 * @return 				Returns an error type errQUEUE_EMPTY if a message was not received from the
 * 						mailbox or a pdPASS if the receive was successful.
 */

portBASE_TYPE xLogicRecieve (xLogicMessage *xMessage, portTickType xDelay);

#endif /* LOGIC_H_ */
