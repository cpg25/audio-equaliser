/**
 * @file 	audiosampler.c
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	5 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 * This module controls the sampling of an audio input. It implements a ping-pong buffer structure
 * for each audio channel. The module is configured for use on the Stellaris LM3S Evaluation Kit.
 */

#include "audiosampler.h"
#include "equaliser.h"

#include "FreeRTOS.h"
#include "queue.h"

#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_types.h"
#include "sysctl.h"
#include "gpio.h"
#include "adc.h"
#include "timer.h"

#include "mailbox.h"

unsigned short xChannel1InputbufferA[BUFFER_LEN];
unsigned short xChannel2InputbufferA[BUFFER_LEN];
unsigned short xChannel1InputbufferB[BUFFER_LEN];
unsigned short XChannel2InputbufferB[BUFFER_LEN];

unsigned long ulADCval[8];
int currentSample = 0;
tBoolean xIsBufferA = true;

portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
xADCMessage xMessage;

xQueueHandle xADCQueue;

void vAudiosamplerIntHandler( void )
{
	// Fetch the data from the ADC.
	ADCSequenceDataGet(ADC_BASE, 1, &(ulADCval[0]));
	if (xIsBufferA)
	{
		// If the current buffer is buffer A fill the the channel one and
		// channel two input buffers. Set the channel one and channel two
		// pointers to these buffers.
		xChannel1InputbufferA[currentSample] = (unsigned short)ulADCval[0];
		xChannel2InputbufferA[currentSample] = (unsigned short)ulADCval[1];
		xMessage.ptrch1 = xChannel1InputbufferA;
		xMessage.ptrch2 = xChannel2InputbufferA;
	}
	else
	{
		// If the current buffer is buffer B fill the the channel one and
		// channel two input buffers. Set the channel one and channel two
		// pointers to these buffers.
		xChannel1InputbufferB[currentSample] = (unsigned short)ulADCval[0];
		XChannel2InputbufferB[currentSample] = (unsigned short)ulADCval[1];
		xMessage.ptrch1 = xChannel1InputbufferB;
		xMessage.ptrch2 = XChannel2InputbufferB;

	}
	currentSample++;
	// Once the buffer length has been reached send of a message to another module
	// Indicating a buffers are ready for processing. Toggle the current buffers.
	if (currentSample == BUFFER_LEN)
	{
		currentSample = 0;
		xHigherPriorityTaskWoken = pdFALSE;
		xQueueSendFromISR( xADCQueue, &xMessage, &xHigherPriorityTaskWoken );
		xIsBufferA = !xIsBufferA;
	}

	// Clear the interrupt flag.
	ADCIntClear(ADC_BASE, 1);

}

void vAudiosamplerInit( void )
{
	// Enable the ADC hardware
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC);

	// Set the ADC speed
	//SysCtlADCSpeedSet(SYSCTL_ADCSPEED_500KSPS);

	// Configure the pin as analogue input
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinTypeADC(GPIO_PORTB_BASE, GPIO_PIN_1); 	//ADC0
	GPIOPinTypeADC(GPIO_PORTA_BASE, GPIO_PIN_4);	//ADC7

	// for 4 ADCs
	// use Sample Sequencer 1 as it can handle up to four ADCs.
	ADCSequenceDisable(ADC_BASE, 1);

	/****************************************************************/
	int frequency = SAMPLE_RATE;

	// Enable the TIMER0 perihperal for pacing our program.
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

	// Configure the 32 bit timer to count down to zero, and to repeat
	// periodically instead of stopping at the maximum value.
	TimerConfigure(TIMER0_BASE, TIMER_CFG_32_BIT_PER);

	// Set the timer's load value. This is the value it will start at,
	// and be reset to.
	TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet()/frequency);

	// Set Timer0 A to trigger the ADC.
    TimerControlTrigger(TIMER0_BASE, TIMER_A, pdTRUE);

	// Enable the timer.
	TimerEnable(TIMER0_BASE, TIMER_A);
	/********************************************************************/


	// configure Sequencer to trigger from processor with priority 0
	ADCSequenceConfigure(ADC_BASE, 1, ADC_TRIGGER_TIMER, 0);

	// configure the steps of the Sequencer
	ADCSequenceStepConfigure(ADC_BASE, 1, 0, ADC_CTL_CH0);
	ADCSequenceStepConfigure(ADC_BASE, 1, 1, ADC_CTL_CH7 | ADC_CTL_IE | ADC_CTL_END);

	// Enable the Sequencer then clear interrupts
	ADCSequenceEnable(ADC_BASE, 1);
	ADCIntClear(ADC_BASE, 1);

	// For dynamically set up interrupts use the code below. For interrupts set up before
	// Runtime use the statup.c file to edit the NVIC. In this case the ADC is static so
	// the interrupt is defined in the startup.c file.
	// ADCIntRegister(ADC_BASE, 1, vADCIntHandler);
	// ADCIntEnable(ADC_BASE, 1);
	// or alternatively
	// IntRegister(INT_ADC1, vADCIntHandler);
	// ADCIntEnable(ADC_BASE, 1);
	// IntEnable(INT_ADC1);

	// For static interrupts use this code. Remember to edit the startup.c file.
	ADCIntEnable(ADC_BASE, 1);
	IntEnable(INT_ADC1);

	//Create a a message queue.
	xADCQueue = xQueueCreate ( mailboxADC_OUTBOX_LEN, mailboxADC_OUTBOX_SIZE );

}

//Add a message to the ADC queue.
portBASE_TYPE xADCSend (xADCMessage *xMessage, portTickType xDelay)
{
	return xQueueSend(xADCQueue, xMessage, xDelay);
}

//Recieve a message from the ADC queue.
portBASE_TYPE xADCRecieve (xADCMessage *xMessage, portTickType xDelay)
{
	xQueueReceive(xADCQueue, xMessage, xDelay);
}
