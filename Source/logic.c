/**
 * @file 	logic.c
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	6 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 * This module controls all the logic controls of the program. It can be considred the gateway
 * task, taking inputs from modules and passing the the resultant output onto other tasks.
 */

#include "logic.h"
#include "equaliser.h"

#include "FREERTOS.h"
#include "queue.h"

#include "button.h"
#include "display.h"
#include "message.h"
#include "mailbox.h"

/* Function Declarations */
void prvIncrementAttenuation( void );
void prvDecrementAttenuation( void );
void prvIncrementCol( void );
void prvDecrementCol( void );

xDisplayMessage xCurrentState;
xLogicMessage xMessageLogic;
portLONG plAttenuationVals[NUM_EQ_COLS];
portLONG plAttenuationValsFFT[NUM_EQ_COLS];

xQueueHandle xLogicQueue;

void vLogicInit (void)
{
	// Create the Button Messenger
	xLogicQueue = xQueueCreate ( mailboxLOGIC_OUTBOX_LEN, mailboxLOGIC_OUTBOX_SIZE );

	xCurrentState.plSelectedCol = 0;
	portLONG i = 0;
	/* Set all the attenuation values to 0 */
	for(i = 0 ; i < NUM_EQ_COLS; i++) plAttenuationVals[i] = ATTENUATION_MAX;
	/* Set a separate array of attenuation values for the FFT to 0 */
	for(i = 0 ; i < NUM_EQ_COLS; i++) plAttenuationValsFFT[i] = ATTENUATION_MAX;
	/* Set the current state pointer to this buffer */
	xCurrentState.plAttenuationVals = plAttenuationVals;
	/* Set the message logic attenuation value pointer to these vaules */
	xMessageLogic.plAttenuationVals = plAttenuationValsFFT;

	/* Set the current channel to channel_ONE */
	xCurrentState.plChannel = CHANNEL_ONE;
	xMessageLogic.plChannel = CHANNEL_ONE;
}

void vLogicControlTask ( void *pvParameters )
{
	xButtonMessage xMessageButton;

	/* Tell the next tasks the current state of the buttons */
	xDisplaySend (&xCurrentState, 0);
	for (;;)
	{
		/* Block until a button press is received */
		xButtonRecieve(&xMessageButton,portMAX_DELAY);

		switch (xMessageButton.button)
		{
		case BUTTON_UP:
			if (xMessageButton.state)
			{
				prvIncrementAttenuation();
			}
			break;
		case BUTTON_DOWN:
			if (xMessageButton.state)
			{
				prvDecrementAttenuation();
			}
			break;
		case BUTTON_LEFT:
			if (xMessageButton.state)
			{
				prvDecrementCol();
			}
			break;
		case BUTTON_RIGHT:
			if (xMessageButton.state)
			{
				prvIncrementCol();
			}
			break;
		case BUTTON_SELECT:
			if (xMessageButton.state)
			{
				xCurrentState.plChannel = !xCurrentState.plChannel;
				xMessageLogic.plChannel = !xMessageLogic.plChannel;
			}
			break;
		}
		if (xMessageButton.state)
		{
			xLogicSend(&xMessageLogic, 0);
			xDisplaySend (&xCurrentState, 0);
		}
	}
}

void prvIncrementAttenuation( void )
{
	/* If the current attenuation value is not maximum increment it. */
	if (xCurrentState.plAttenuationVals[xCurrentState.plSelectedCol] != ATTENUATION_MAX)
	{
		xCurrentState.plAttenuationVals[xCurrentState.plSelectedCol] += ATTENUATION_STEP;
		xMessageLogic.plAttenuationVals[xCurrentState.plSelectedCol] += ATTENUATION_STEP;
	}
}

void prvDecrementAttenuation( void )
{
	/* If the current attenuation value is not the minimum decrement it. */
	if (xCurrentState.plAttenuationVals[xCurrentState.plSelectedCol] != ATTENUATION_MIN)
	{
		xCurrentState.plAttenuationVals[xCurrentState.plSelectedCol] -= ATTENUATION_STEP;
		xMessageLogic.plAttenuationVals[xCurrentState.plSelectedCol] -= ATTENUATION_STEP;
	}
}

void prvIncrementCol( void )
{
	/* If the current column is the last column, wrap around the screen, */
	/* otherwise increment the selected column.							 */
	if (xCurrentState.plSelectedCol == NUM_EQ_COLS-1)
	{
		xCurrentState.plSelectedCol = 0;
	}
	else
	{
		xCurrentState.plSelectedCol++;
	}
}

void prvDecrementCol( void )
{
	/* If the current column is the first column, wrap around the screen, */
	/* otherwise increment the selected column.							  */
	if (xCurrentState.plSelectedCol == 0)
	{
		xCurrentState.plSelectedCol = NUM_EQ_COLS-1;
	}
	else
	{
		xCurrentState.plSelectedCol--;
	}
}

portBASE_TYPE xLogicSend (xLogicMessage *xMessage, portTickType xDelay)
{
	return xQueueSend(xLogicQueue, xMessage, xDelay);
}

portBASE_TYPE xLogicRecieve (xLogicMessage *xMessage, portTickType xDelay)
{
	return xQueueReceive(xLogicQueue, xMessage, xDelay);
}
