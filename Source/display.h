/**
 * @file 	display.h
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	9 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 * This module controls the display on the LM3S1968. It recieves messages from the FFT module
 * and the logic controller and displays the appropriate equalisation output.
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include "FREERTOS.h"
#include "message.h"

 /**
  * @def    NUM_EQ_ROWS
  * @breif 	This specifies the number of rows that the equaliser should show on screen. By defining this
  * 		the user does not need to worry about changing the display manually.
  */
#define NUM_EQ_ROWS  10

 /**
  * @def    NUM_EQ_COLS
  * @breif 	This specifies the number of columns that the equaliser should show on screen. By defining this
  * 		the user does not need to worry about changing the display manually.
  */
#define NUM_EQ_COLS  8

/**
 * Intialises the OLED display.
 */
void vDisplayInit (void);

/**
 * The display task organises all icoming messages into an equaliser display.
 * @param 	pvParameters Parameters can be passed into the task when the task is created in FREERTOS. Currently
 * 			                  this task will ignore any parameters
 */
void vDisplayTask( void *pvParameters );


/**
 * Sends a message to the Display module indicating that a screen update is required.
 * 
 * @param xMessage		A message struct containing a button and the current state of the equalisation.
 * 						The format is specified in the message.h header file.
 * 	
 * @param xDelay		A value for how long the the function is to wait for a message to arrive in the Display
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a message is
 * 						received. A value of 0 will return immediately regardless of whether or not the
 * 						message was received.
 * 						
 * @return 				Returns an error type errQUEUE_FULL if a message was not received from the
 * 						mailbox or a pdPASS if the receive was successful.
 */

portBASE_TYPE xDisplaySend (xDisplayMessage *xMessage, portTickType xDelay);

/**
 * Recieves a message in the Display module indicating that a screen update is required.
 * 
 * @param xMessage		A message struct containing a button and the current state of the equalisation.
 * 						The format is specified in the message.h header file.
 * 	
 * @param xDelay		A value for how long the the function is to wait for a message to arrive in the Display
 * 						mailbox before returning. portMAX_DELAY will wait indefinitely until a message is
 * 						received. A value of 0 will return immediately regardless of whether or not the
 * 						message was received.
 * 						
 * @return 				Returns an error type errQUEUE_EMPTY if a message was not received from the
 * 						mailbox or a pdPASS if the receive was successful.
 */

portBASE_TYPE xDisplayRecieve (xDisplayMessage *xMessage, portTickType xDelay);

#endif /* DISPLAY_H_ */
