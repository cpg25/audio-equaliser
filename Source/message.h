/**
 * @file 	message.h
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	4 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 *  The different messenger formats avaiable.
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include "FREERTOS.h"

#include "hw_types.h"

/**
 * @struct 	xDisplayMessage
 * @brief	Struct containing information required for the display module messenger
 */
typedef struct
{
	portLONG *plAttenuationVals; 	/**< the attenuation values of the display */
	portLONG plSelectedCol;			/**< The currently selected column by the user */
	portLONG plChannel;				/**< The audio channel currently on display */
} xDisplayMessage;					
/**
 * @struct 	xLogicMessage
 * @brief	Struct containing information required for the Logic messenger
 */
typedef struct
{
	portLONG *plAttenuationVals; 	/**< the attenuation values of the user input */
	portLONG plChannel;				/**< The audio channel currently on selected */
} xLogicMessage;

/**
 * @struct 	xButtonMessage
 * @brief	Struct containing information required for the Button messenger
 */
typedef struct
{
	char button;				/**< The current button */
	tBoolean state;				/**< The state of that button (Pressed/Released) */
} xButtonMessage;

/**
 * @struct 	xADCMessage
 * @brief	Struct containing information required for the ADC messenger
 */
typedef struct
{
	void *ptrch1;			/**< pointer to the channel 1 audio stream */
	void *ptrch2;			/**< pointer to the channel 2 audio stream */
} xADCMessage;

/**
 * @struct 	xSpeakerMessage
 * @brief	Struct containing information required for the Speaker messenger
 */
typedef struct
{
	void *ptraudio;			/**< pointer to the current audio stream */
} xSpeakerMessage;

/**
 * @struct 	xFFTMessage
 * @brief	Struct containing information required for the FFT messenger
 */
typedef struct
{
	portLONG* plFreqVals;	/**< The current values of the FFT */
} xFFTMessage;

#endif /* MESSAGE_H_ */
