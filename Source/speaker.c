/**
 * @file 	speaker.c
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	7 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 *  This module is in charge of playing the speaker.
 */
#include "speaker.h"
#include "equaliser.h"

#include "mailbox.h"
#include "message.h"

#include "FREERTOS.h"
#include "queue.h"
#include "class-d-adam.h"

#include "string.h"

#define DECIMATION 8

//Button Messenger
xQueueHandle xSpeakerQueue;

unsigned short* audio;
unsigned char audio2[BUFFER_LEN];

void vDecimate(int *audio, int* outputaudio , int audiolen, int decimationFactor);

void vSpeakerInit ( void )
{
	// Intialise the Class-D amplifier
	ClassDInit(SysCtlClockGet());

	xSpeakerQueue = xQueueCreate ( mailboxSPEAKER_INBOX_LEN, mailboxSPEAKER_INBOX_SIZE);
}

void vSpeakerTask (void *pvParameters )
{
	ClassDVolumeSet(256); //256 MAX
	int i;
	xSpeakerMessage xMessage;
	for ( ;; )
	{
		vSpeakerRecieve(&xMessage,portMAX_DELAY);
		audio = xMessage.ptraudio;

		for (i = 0; i < BUFFER_LEN; i++) audio2[i] = (unsigned char)(audio[i]>>2);

		//Play the audio Buffer.
		ClassDPlayPCMRealTime(audio2, BUFFER_LEN);


	}
}
void vDecimate (int *audio, int* outputaudio , int audiolen, int decimationFactor)
{
	int i= 0;
	int audioindex = 0;
	for (i = 0; i < audiolen; i+=decimationFactor)
	{
		outputaudio[audioindex] = audio[i];
		audioindex++;
	}
}

void vSpeakerSend (xSpeakerMessage *xMessage, portTickType xDelay)
{
	xQueueSend(xSpeakerQueue, xMessage, xDelay);
}

void vSpeakerRecieve (xSpeakerMessage *xMessage, portTickType xDelay)
{
	xQueueReceive(xSpeakerQueue, xMessage, xDelay);
}
