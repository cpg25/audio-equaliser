/**
 * @file 	equaliser.h
 *
 * @author 	Craig Gray <cpg25@uclive.ac.nz>
 * @date 	7 / 8 / 2013
 *
 * @copyright	Craig Gray, 2013
 * @license 	Released under the MIT license, avalible here: http://opensource.org/licenses/MIT
 *
 *  This program implements an audio equaliser on the Stellaris LM3S1968 Evaluation board. The
 *  program is scheduled using the FREERTOS real time operating system. The program functions within
 *  6 core modules. 
 *
 *	Modules:
 *	audiosampler 	- In charge of sampling and ping-pong buffering
 *	FFT				- In charge of the digital signal processing of the system
 *	button			- In charge of user inputs to the systems
 *	logic			- In charge of interpreting user inputs and send the appropriate messages out
 *	speaker			- In charge of playing audio out after processing
 *	display			- In charge of displaying the current state of the program to the user
 *
 * These modules can be found in the functional block diagram included.
 */


#ifndef EQUALISER_H_
#define EQUALISER_H_

/**
 * @def SAMPLE_RATE
 * @breif The rate the auido equaliser
 */
#define SAMPLE_RATE 	24000
 /**
 * @def INTERPOLATION
 * @breif Whether or not the output should be interpolated up.
 */
#define INTERPOLATION	0
  /**
 * @def BUFFER_LEN
 * @breif The length of the sampling buffer and FFT buffers
 */
#define BUFFER_LEN 		128

#define CHANNEL_ONE 	1
#define CHANNEL_TWO		0

#endif /* EQUALISER_H_ */
